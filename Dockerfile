FROM python:3.7-alpine

WORKDIR /app/

COPY /code/app.py /app/app.py
COPY /static/ /app/static/
COPY /templates/ /app/templates/

RUN pip install flask
RUN pip install requests

ENTRYPOINT ["python3", "/app/app.py"]